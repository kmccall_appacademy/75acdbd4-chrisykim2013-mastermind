class Code
  attr_reader :pegs

  PEGS = {
    "r" => "red",
    "g" => "green",
    "b" => "blue",
    "y" => "yellow",
    "o" => "orange",
    "p" => "purple"
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def [](key)
    @pegs[key]
  end

  def exact_matches(other_code)
    count = 0
    @pegs.each.with_index do |c1, i1|
      other_code.pegs.each.with_index {|c2, i2| count += 1 if c1 == c2 && i1 == i2}
    end
    count
  end

  def near_matches(other_code)
    new_self = []
    new_other = []
    @pegs.each.with_index {|c, i| new_self << @pegs[i] unless @pegs[i] == other_code[i]}
    other_code.pegs.each.with_index {|c, i| new_other << other_code[i] unless @pegs[i] == other_code[i]}
    count = 0
    new_other.each.with_index do |c1, i1|
      new_self.each.with_index do |c2, i2|
        if c1 == c2
          count += 1
          new_self.delete_at(i2)
          break
        end
      end
    end
    count
  end

  def ==(other_code)
    other_code.is_a?(Code) && @pegs == other_code.pegs
  end

  def self.parse(str)
    peg_arr = str.downcase.chars
    peg_arr.all? {|c| PEGS.include?(c)} ? Code.new(peg_arr) : raise("Error!")
  end

  def self.random
    rand_code = [nil, nil, nil, nil].map {|c| PEGS.keys.sample}
    Code.new(rand_code)
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    begin
      puts "Please provide your best guess in the format XXXX!"
      guess = gets.chomp
      @guess = Code.parse(guess)
    rescue
      puts "Invalid entry. Please provide another guess."
      retry
    end
  end

  def display_matches(other_code)
    exact = @secret_code.exact_matches(other_code)
    puts "There are #{exact} exact matches!"
    near = @secret_code.near_matches(other_code)
    puts "There are also #{near} near matches!"
  end

  def play
    round = 1
    until win? || round == 11
      puts "This is round #{round}!"
      get_guess
      display_matches(@guess)
      round += 1
    end
    if win?
      puts "Congrats! You won the game!"
    else
      puts "Sadly, you used up all of your chances. Try again!" unless win?
      puts "The correct answer was #{@secret_code.pegs}."
    end
  end

  def win?
    @secret_code == @guess
  end
end

if __FILE__ == $PROGRAM_NAME
  new_game = Game.new
  new_game.play
end
